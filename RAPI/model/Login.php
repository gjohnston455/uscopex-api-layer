<?php
    class Login{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
        public function login($Email,$Passwd)
        {   
            $authentication = $this->conn->prepare('SELECT ID FROM Login WHERE Email = ? AND Password = ?');
            $authentication-> bind_param("ss",$Email,$Passwd);
            if($authentication->execute()){

                $authentication -> store_result();
                $numrows =$authentication->num_rows; 
                $authentication -> bind_result($Login_ID);  
                $authentication->close();

                if($numrows>0){
                    return SUCCESS;
                }else{
                    return DENIED;
                }
            }else{
                return FAILURE;
            }

        }
        public function getUserDetails($Email){
            $authentication2 = $this->conn->prepare('SELECT a.First_Name, a.Last_Name, a.ID as Account_id, l.ID as login_id 
            FROM `Account_Information`AS a 
            LEFT JOIN Login l on a.Login_ID = l.ID 
            WHERE Email = ?');
            $authentication2->bind_param("s",$Email);
            $authentication2->execute();
            $authentication2->store_result();
            $authentication2->bind_result($First_Name,$Last_Name,$account_id,$login_id);
            $authentication2->fetch();
            
          
            $user_info = array();
            $user_info['accountId'] = $account_id;
            $user_info['firstName'] = $First_Name;
            $user_info['lastName'] = $Last_Name;
            $user_info['email'] = $Email;
            $user_info['loginId'] = $login_id;
            return $user_info;
        }
    }

?>
