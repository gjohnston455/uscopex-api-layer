<?php
    class IcmResults{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getResultSets($Login_ID){
            $getResults = $this->conn->prepare('SELECT
            r.ID as iD,r.Name as name,
            r.Classification as classification,
            DATE_FORMAT(r.Date,"%d/%m/%y") AS date,
            r.Description as description,
            m.Name as modelName,
            m.ClassNames as classNames
            FROM ICM_Result r 
            INNER JOIN Model m on r.Model = m.ID
            WHERE r.User_ID = ?
            ORDER BY r.ID DESC');
         
       
            $getResults->bind_param("i",$Login_ID);
            $getResults->execute();
            $resultArray = array();
            $result = $getResults->get_result();
            while ($row = $result->fetch_assoc()){
                $resultArray[] = $row;
            } 
            $getResults->close();
            return $resultArray;
        }
    }

?>
