<?php
    class DiscoverPipelineVersions{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getPipelineVersions($pipeline_id){
            $getPipelineVersions = $this->conn->prepare('SELECT pv.ID as iD,pv.Version as version ,pv.Description as description,DATE_FORMAT(pv.Date,"%d/%m/%y") AS date,
            po.Image as producingImage,
            po.Result_Set as producingResults,
            pr.Requires_Roi as requiresRoi,
            pr.Requires_Scale as requiresScale
            FROM Pipeline_Version pv
            INNER JOIN Pipeline p ON pv.Pipeline = p.ID
            INNER JOIN Pipeline_Output po ON pv.ID = po.Pipeline
            INNER JOIN Pipeline_Requirements pr ON pv.ID = pr.Pipeline_ID
            WHERE p.ID =  ?
            ORDER BY ID DESC');

            $getPipelineVersions->bind_param("i",$pipeline_id);
            $getPipelineVersions->execute();
            $pipelineVersionInfo = array();
            $result = $getPipelineVersions->get_result();
            while ($row = $result->fetch_assoc()){
                $pipelineVersionInfo[] = $row;
            } 
            $getPipelineVersions->close();
            return $pipelineVersionInfo;
          
        }
     
    }

?>
