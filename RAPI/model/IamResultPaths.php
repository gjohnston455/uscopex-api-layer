<?php
    class IamResultPaths{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getImagePaths($resultSetId){
            $getImagePaths = $this->conn->prepare('Select 
            i.Path AS imagePath
            FROM iam_result_set s 
            LEFT JOIN iam_result_image_path i on s.ID = i.Result_Set
            WHERE s.ID =?');

            $getImagePaths->bind_param("i",$resultSetId);
            $getImagePaths->execute();
            $imagePathArray = array();
            $result = $getImagePaths->get_result();
            while ($row = $result->fetch_assoc()){
                $imagePathArray[] = $row;
            } 
            $getImagePaths->close();
            return $imagePathArray;
        }
        public function getResultPaths($resultSetId){
            $getResultPaths = $this->conn->prepare('Select 
            r.Path AS resultPath
            FROM iam_result_set s 
            LEFT JOIN iam_result_path r on s.ID = r.Result_Set
            WHERE s.ID =?');

            $getResultPaths->bind_param("i",$resultSetId);
            $getResultPaths->execute();
            $resultPathArray = array();
            $result = $getResultPaths->get_result();
            while ($row = $result->fetch_assoc()){
                $resultPathArray[] = $row;
            } 
            $getResultPaths->close();
            return $resultPathArray;
        }
    }

?>
