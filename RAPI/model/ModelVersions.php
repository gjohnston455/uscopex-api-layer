<?php
    class ModelVersions{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getVersions($model_id){
            $getVersions = $this->conn->prepare('SELECT Model,Version, Description, Date
            FROM Model_Version 
            WHERE Model = ?
            ');

            $getVersions->bind_param("i",$model_id);
            $getVersions->execute();
            $versionArray = array();
            $result = $getVersions->get_result();
            while ($row = $result->fetch_assoc()){
                $versionArray[] = $row;
            } 
            $getVersions->close();
            return $versionArray;
        }
     
    }

?>