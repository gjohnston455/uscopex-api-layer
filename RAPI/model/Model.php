<?php
    class Model{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
        public function checkAccess($login_id)
        {   
            $checkAccess = $this->conn->prepare('SELECT  COUNT(*)
            FROM Model a 
            LEFT JOIN Model_Access p ON a.ID = p.Model_ID
            WHERE p.User_ID = ? OR a.Is_Public=1
            ');
            $checkAccess-> bind_param("i",$login_id);
            if($checkAccess->execute()){

                $checkAccess -> store_result();
                $checkAccess -> bind_result($count);  
                $checkAccess->fetch();
                $checkAccess->close();

                if($count>0){
                    return SUCCESS;
                }else{
                    echo "Problem";
                    return DENIED;
                }
            }else{
                return FAILURE;
            }

        }
        public function getModels($Login_ID){
            $getPipelines = $this->conn->prepare('SELECT a.ID, a.Name, a.Description,
           a.ClassNames, a.Date_Added, x.FIrst_Name AS author_name
            FROM Model a 
            LEFT JOIN Model_Access p ON a.ID = p.Model_ID
            LEFT JOIN Login l ON a.Author = l.ID
            LEFT JOIN Account_Information x ON l.ID = x.Login_ID
            WHERE p.User_ID = ? OR a.Is_Public = 1
            ');

            $getPipelines->bind_param("i",$Login_ID);
            $getPipelines->execute();
            $usersArr = array();
            $result = $getPipelines->get_result();
            while ($row = $result->fetch_assoc()){
                $usersArr[] = $row;
            } 
            $getPipelines->close();
            return $usersArr;
          
        }



     
    }

?>