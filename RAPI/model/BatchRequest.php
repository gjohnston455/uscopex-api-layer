<?php
    class BatchRequest{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function insertRequest($processName,$pipline,$description,$userid){

            if(!$this->checkNameFree($processName,$userid)>0){
            $insertRequest = $this->conn->prepare('
            INSERT INTO iam_result_set 
            (Name, Date, User_id, Pipeline, Description, Is_Batch)
             VALUES (?, NOW(),?,?,?,1)');

            $insertRequest->bind_param("siis",$processName,$userid,$pipline,$description);
            if($insertRequest->execute()){
                $requestId = $this->conn->insert_id;
                $insertRequest->close();
                return $requestId;
            }
        }else{
            return ALREADY_EXISTS;
        }
           
          
        }

        public function checkNameFree($processName,$userid){
            $validate = $this->conn->prepare('SELECT * FROM iam_result_set WHERE Name = ? AND Is_Batch =1 AND User_id = ?');
            $validate->bind_param("si",$processName,$userid);
            
            $validate->execute();
            $validate->store_result();
            return $validate ->num_rows;
        }



     
    }

?>
