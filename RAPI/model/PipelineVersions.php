<?php
    class PipelineVersions{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getVersions($pipeline_id){
            $getVersions = $this->conn->prepare('SELECT 
            p.ID,p.Pipeline,p.Version, p.Description, p.Date, 
            r.Requires_Roi,r.Requires_Scale, o.Image,o.Result_Set
            
            FROM Pipeline_Version p
            INNER JOIN Pipeline_Requirements r ON p.ID = r.Pipeline_ID
            INNER JOIN Pipeline_Output o ON p.ID = o.Pipeline
            WHERE p.Pipeline = ?
            ');

            $getVersions->bind_param("i",$pipeline_id);
            $getVersions->execute();
            $versionArray = array();
            $result = $getVersions->get_result();
            while ($row = $result->fetch_assoc()){
                $versionArray[] = $row;
            } 
            $getVersions->close();
            return $versionArray;
        }
     
     
    }

?>