<?php
    class AnalysisNameValidation{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }


        public function checkNameFree($processName,$userid){
            $isBatch = 0;
            $validate = $this->conn->prepare('SELECT * FROM iam_result_set WHERE Name = ? AND User_id = ? AND Is_Batch =?');
            $validate->bind_param("sii",$processName,$userid,$isBatch);
            
            $validate->execute();
            $validate->store_result();
            $num_rows = $validate ->num_rows;
            $validate->close();
            return $num_rows;
        }



     
    }

?>
