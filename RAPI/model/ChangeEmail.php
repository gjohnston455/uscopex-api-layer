<?php
class ChangeEmail
{

    private $conn;

    public function __construct()
    {
        require_once '../dbconfig/Database.php';
        include_once '../model/responses.php';
        $db = new Database;
        $this->conn = $db->connection();
    }


    public function changeEmail($passwd, $userId, $email)
    {
        if ($this->passwordValidation($userId, $passwd) > 0) {

            $change_email = $this->conn->prepare('UPDATE Login SET Email = ? WHERE ID = ?');
            $change_email->bind_param("si", $email, $userId);
            if ($change_email->execute()) {
                return CREATED;
            } else {
                return FAILURE;
            }
            return FAILURE;
        } else {
            return DENIED;
        }
    }
    public function passwordValidation($userId, $passwd)
    {
        $passwd_validation = $this->conn->prepare('SELECT * FROM Login WHERE ID = ? AND Password = ?');
        $passwd_validation->bind_param("is", $userId, $passwd);
        $passwd_validation->execute();
        $passwd_validation->store_result();
        return $passwd_validation->num_rows;
    }
}
