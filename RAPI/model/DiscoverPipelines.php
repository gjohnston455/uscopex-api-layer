<?php
    class DiscoverPipelines{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getPipelines($Login_ID){
            $getPipelines = $this->conn->prepare('SELECT p.ID as iD,p.Name as name, p.Versions as versions, p.Description as description, p.Batchable as batchable,
            CONCAT(a.FIrst_Name," ",a.Last_Name) AS author 
            FROM Pipeline p
            INNER JOIN Login l ON p.Author = l.ID
        	INNER JOIN Account_Information a on l.ID = a.Login_ID
            INNER JOIN Pipeline_Access pa on p.ID = pa.Pipeline_ID
            WHERE Is_Public = 1 OR pa.User_ID = ?');

            $getPipelines->bind_param("i",$Login_ID);
            $getPipelines->execute();
            $pipelineInfo = array();
            $result = $getPipelines->get_result();
            while ($row = $result->fetch_assoc()){
                $pipelineInfo[] = $row;
            } 
            $getPipelines->close();
            return $pipelineInfo;
          
        }
     
    }

?>
