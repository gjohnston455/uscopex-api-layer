<?php
    class IamResults{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getResultSets($Login_ID,$Is_Batch){

            if($Is_Batch == 0){
            $getResults = $this->conn->prepare('SELECT 
            a.ID AS iD, a.Name AS name, 
                        DATE_FORMAT(a.Date,"%d/%m/%y") AS date,
                        p.Name AS pipeline,
                        pv.Version AS pipelineVersion, 
                        a.Description AS description,
                        i.Path AS imagePath,
                        r.Path AS resultPath
                        FROM iam_result_set a 
                        LEFT JOIN iam_result_image_path i on a.ID = i.Result_Set
                        LEFT JOIN iam_result_path r on a.ID = r.Result_Set
                        LEFT JOIN Pipeline_Version pv ON a.Pipeline = pv.ID
                        LEFT JOIN Pipeline p ON pv.Pipeline = p.ID
                        WHERE a.User_ID = ? AND a.Is_Batch = ?');
            }else{
                $getResults = $this->conn->prepare('SELECT 
                a.ID AS iD, a.Name AS name, 
                DATE_FORMAT(a.Date,"%d/%m/%y") AS date,
                p.Name AS pipeline,
                pv.Version AS pipelineVersion, 
                a.Description AS description
                FROM iam_result_set a 
                LEFT JOIN Pipeline_Version pv ON a.Pipeline = pv.ID
                LEFT JOIN Pipeline p ON pv.Pipeline = p.ID
                WHERE a.User_ID = ? AND a.Is_Batch = ?');
            }
            $getResults->bind_param("ii",$Login_ID,$Is_Batch);
            $getResults->execute();
            $resultArray = array();
            $result = $getResults->get_result();
            while ($row = $result->fetch_assoc()){
                $resultArray[] = $row;
            } 
            $getResults->close();
            return $resultArray;
        }
    }

?>
