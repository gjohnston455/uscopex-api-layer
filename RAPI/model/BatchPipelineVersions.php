<?php
    class BatchPipelineVersions{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getVersions($pipeline_id){
            $getVersions = $this->conn->prepare('SELECT Pipeline,Version, Description, Date
            FROM Batch_Pipeline_Version 
            WHERE Pipeline = ?
            ');

            $getVersions->bind_param("i",$pipeline_id);
            $getVersions->execute();
            $versionArray = array();
            $result = $getVersions->get_result();
            while ($row = $result->fetch_assoc()){
                $versionArray[] = $row;
            } 
            $getVersions->close();
            return $versionArray;
        }
     
    }

?>