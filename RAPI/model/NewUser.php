<?php
    class NewUser{

        private $conn;
     
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
        public function newUser($FirstName,$LastName,$Email,$Passwd)
        {   
            if(!$this->emailValidation($Email)>0){
                $login_id = $this->insertLoginTable($Email,$Passwd);
            
                if($login_id>0){
                    $insert_user = $this->conn->prepare('INSERT INTO Account_Information (login_ID,First_Name,Last_Name) VALUES (?,?,?)');
                    $insert_user->bind_param("iss",$login_id,$FirstName,$LastName);
                    if($insert_user->execute()){
                        return CREATED;
                    }else{
                        return FAILURE;
                    }
                }
                return FAILURE;
            }else{
                
            return ALREADY_EXISTS;
            }

        }
        public function emailValidation($Email){
            $email_val = $this->conn->prepare('SELECT * FROM Login WHERE Email = ?');
            $email_val->bind_param("s",$Email);
            $email_val->execute();
            $email_val->store_result();
            return $email_val ->num_rows;
           
        }

        public function insertLoginTable($Email,$Passwd){
            $insert_login = $this->conn->prepare('INSERT INTO Login
            (Email,Password)
            VALUES(?,?)');
            $insert_login->bind_param("ss",$Email,$Passwd);
            if($insert_login->execute()){
                
               $id = $this->conn->insert_id;
               
               return $id;
            }else{
                echo "Failed to insert";
                return null;
            }
        }
    }
?>