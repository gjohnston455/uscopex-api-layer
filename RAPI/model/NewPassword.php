<?php
    class NewPassword{

        private $conn;
     
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }


        public function changePassword($newPassword, $userId, $currentPassword)
        {
            if ($this->passwordValidation($userId, $currentPassword) > 0) {
    
                $change_password = $this->conn->prepare('UPDATE Login SET Password = ? WHERE ID = ?');
                $change_password->bind_param("si", $newPassword, $userId);
                if ($change_password->execute()) {
                    return CREATED;
                } else {
                    return FAILURE;
                }
                return FAILURE;
            } else {
                return DENIED;
            }
        }
        public function passwordValidation($userId, $currentPassword)
        {
            $passwd_validation = $this->conn->prepare('SELECT * FROM Login WHERE ID = ? AND Password = ?');
            $passwd_validation->bind_param("is", $userId, $currentPassword);
            $passwd_validation->execute();
            $passwd_validation->store_result();
            return $passwd_validation->num_rows;
        }
    }
?>