<?php
    class DiscoverModels{
        private $conn;
        public function __construct()
        {
            require_once '../dbconfig/Database.php';
            include_once '../model/responses.php';
            $db = new Database;
            $this->conn = $db->connection();
        }
       
        public function getModelInfo($Login_ID){
            $getModels = $this->conn->prepare('SELECT m.ID as iD, m.Name as name, m.Description as description, CONCAT(ai.FIrst_Name, " " ,ai.Last_Name) AS author, DATE_FORMAT(m.Date_Added,"%d/%m/%y") AS date, m.ClassNames AS classNames
            FROM Model m
            INNER JOIN Model_Access ma ON m.ID = ma.Model_ID
            INNER JOIN Login l ON m.Author = l.ID
            INNER JOIN Account_Information ai ON l.id = ai.Login_ID
            WHERE ma.User_ID = ?
            OR m.Is_Public =1');

            $getModels->bind_param("i",$Login_ID);
            $getModels->execute();
            $modelInfo = array();
            $result = $getModels->get_result();
            while ($row = $result->fetch_assoc()){
                $modelInfo[] = $row;
            } 
            $getModels->close();
            return $modelInfo;
          
        }
     
    }

?>
