<?php
	use Psr\Http\Message\ResponseInterface as Response;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Slim\Factory\AppFactory;

    //require '../uscopex_apis/vendor/autoload.php';
    require __DIR__ . '/../../../vendor/autoload.php';
    require '../model/NewUser.php';
    require '../model/Login.php';
	require '../model/Pipeline.php';
	require '../model/Model.php';
	require '../model/PipelineVersions.php';
	require '../model/ModelVersions.php';
	require '../model/BatchPipelineVersions.php';
	require '../model/BatchPipeline.php';
	require '../model/BatchRequest.php';
	require '../model/AnalysisNameValidation.php';
	require '../model/IcmResults.php';
	require '../model/IamResults.php';
	require '../model/IamResultPaths.php';
	require '../model/DiscoverModels.php';
	require '../model/DiscoverPipelines.php';
	require '../model/DiscoverPipelineVersions.php';
	require '../model/ChangeEmail.php';
	require '../model/NewPassword.php';

	$app = AppFactory::create();
    $app->setBasePath("/RAPI/public");
    $app->addRoutingMiddleware();
    $app->addErrorMiddleware(true, true, true);



	$app->post('/createuser', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('Email', 'Passwd', 'FirstName', 'LastName'), $response)) {
			$request_data = $request->getParsedBody();

			$email = $request_data['Email'];
			$password = $request_data['Passwd'];
			$first_name = $request_data['FirstName'];
			$last_name = $request_data['LastName'];

			$hash_password = password_hash($password, PASSWORD_DEFAULT);

			$db = new NewUser;

			$result = $db->newUser($first_name, $last_name, $email, $password);

			if ($result == CREATED) {
				$message = array();
				$message['error'] = false;
				$message['message'] = 'User Created Successfully.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(201);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Some error occurred.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(422);

			} else if ($result == ALREADY_EXISTS) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'User Already Exists.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(422);
			}
		}

		return $response
							->withHeader('Content-type', 'application/json')
							->withStatus(422);
	});


	$app->post('/changeemail', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('email', 'user_id', 'passwd'), $response)) {
			$request_data = $request->getParsedBody();

			$email = $request_data['email'];
			$password = $request_data['passwd'];
			$userId = $request_data['user_id'];
		

			$db = new ChangeEmail;

			$result = $db->changeEmail($password,$userId,$email);

			if ($result == CREATED) {
				$message = array();
				$message['error'] = false;
				$message['message'] = 'Email changed';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(201);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Some error occurred.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(422);

			}
		}

		return $response
							->withHeader('Content-type', 'application/json')
							->withStatus(422);
	});
	
	$app->post('/changepasswd', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('current_passwd', 'new_passwd', 'user_id'), $response)) {
			$request_data = $request->getParsedBody();

			$currentPasswd = $request_data['current_passwd'];
			$newPasswd = $request_data['new_passwd'];
			$userId = $request_data['user_id'];
		

			$db = new NewPassword;

			$result = $db->changePassword($newPasswd,$userId,$currentPasswd);

			if ($result == CREATED) {
				$message = array();
				$message['error'] = false;
				$message['message'] = 'Password changed';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(201);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Some error occurred.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(422);

			}
		}

		return $response
							->withHeader('Content-type', 'application/json')
							->withStatus(422);
    });
	










    $app->post('/login', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('Email', 'Passwd'), $response)) {
			$request_data = $request->getParsedBody();

			$email = $request_data['Email'];
			$password = $request_data['Passwd'];
            $db = new Login;

            $result = $db->login($email, $password);

            if ($result == SUCCESS) {

                $user_info = $db->getUserDetails($email);
            
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Login Success.';
                $message['user'] = $user_info;

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			} else if ($result == DENIED) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Incorrect details';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(401);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Connection error.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
		    						->withStatus(104);
			}
        }
    });

    $app->post('/pipeline', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('login_id'), $response)) {
			$request_data = $request->getParsedBody();

			$login_id = $request_data['login_id'];
    
            $db = new Pipeline;

            $result = $db->checkAccess($login_id);

            if ($result == SUCCESS) {

                $pipeline_info = $db->getPipelines($login_id);
            
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Request Success';
                $message['pipeline'] = $pipeline_info;

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			} else if ($result == DENIED) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'No access provided to user';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(401);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Connection error.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
		    						->withStatus(104);
			}
        }
	});

	$app->post('/pipelineversions', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('pipeline_id'), $response)) {
			$request_data = $request->getParsedBody();

			$pipeline_id = $request_data['pipeline_id'];
    
            $db = new PipelineVersions;

            $result = $db->getVersions($pipeline_id);

				$message = array();
				$message['error'] = false;
                $message['message'] = 'Versions';
                $message['pipeline_versions'] = $result;

				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
        }
	});

	$app->post('/batchpipeline', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('login_id'), $response)) {
			$request_data = $request->getParsedBody();

			$login_id = $request_data['login_id'];
    
            $db = new BatchPipeline;

            $result = $db->checkAccess($login_id);

            if ($result == SUCCESS) {

                $pipeline_info = $db->getPipelines($login_id);
            
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Request Success';
                $message['pipeline'] = $pipeline_info;

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			} else if ($result == DENIED) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'No access provided to user';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(401);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Connection error.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
		    						->withStatus(104);
			}
        }
	});

	$app->post('/batchpipelineversions', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('pipeline_id'), $response)) {
			$request_data = $request->getParsedBody();

			$pipeline_id = $request_data['pipeline_id'];
    
            $db = new BatchPipelineVersions;

            $result = $db->getVersions($pipeline_id);

				$message = array();
				$message['error'] = false;
                $message['message'] = 'Versions';
                $message['pipeline_versions'] = $result;

				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
        }
	});

	
	$app->post('/model', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('login_id'), $response)) {
			$request_data = $request->getParsedBody();

			$login_id = $request_data['login_id'];
    
            $db = new Model;

            $result = $db->checkAccess($login_id);

            if ($result == SUCCESS) {

                $pipeline_info = $db->getModels($login_id);
            
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Request Success';
                $message['model'] = $pipeline_info;

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			} else if ($result == DENIED) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'No access provided to user';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(401);

			} else if ($result == FAILURE) {
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Connection error.';

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
		    						->withStatus(104);
			}
        }
	});


	$app->post('/modelversions', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('model_id'), $response)) {
			$request_data = $request->getParsedBody();

			$model_id = $request_data['model_id'];
    
            $db = new ModelVersions;

            $result = $db->getVersions($model_id);
			
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Versions';
                $message['model_versions'] = $result;

				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
        }
	});

	$app->post('/batchrequest', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('process_name','pipeline','description','user_id'), $response)) {
			$request_data = $request->getParsedBody();

			$processName = $request_data['process_name'];
			$pipline = $request_data['pipeline'];
			$description = $request_data['description'];
			$userid = $request_data['user_id'];
			
            $db = new BatchRequest;

			$result = $db->insertRequest($processName,$pipline,$description,$userid);
			if($result != ALREADY_EXISTS){
			
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Versions';
                $message['request_id'] = $result;

				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
			}else{
				$message = array();
				$message['error'] = true;
				$message['message'] = 'Name already in use';
				$message['request_id'] = -1;

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			}
        }
	});


	$app->post('/analysisnamevalidation', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('process_name','user_id'), $response)) {
			$request_data = $request->getParsedBody();

			$processName = $request_data['process_name'];
			$userid = $request_data['user_id'];
			
            $db = new AnalysisNameValidation;

			$result = $db->checkNameFree($processName,$userid);
			if($result <1){
			
				$message = array();
				$message['error'] = false;
                $message['message'] = 'Available';
              

				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
			}else{
				$message = array();
				$message['error'] = false;
				$message['message'] = 'Name already in use';
				

				$response->getBody()->write(json_encode($message));

				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);

			}
        }
	});

	$app->post('/icmresults', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('user_id'), $response)) {
			$request_data = $request->getParsedBody();

			
			$userid = $request_data['user_id'];
		
			
            $db = new IcmResults;

			$result = $db->getResultSets($userid);
				$message = array();
				$message['error'] = false;
                $message['results'] = $result;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});

	$app->post('/iamresults', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('user_id','is_batch'), $response)) {
			$request_data = $request->getParsedBody();

			
			$userid = $request_data['user_id'];
			$is_batch = $request_data['is_batch'];
			
            $db = new IamResults;

			$result = $db->getResultSets($userid,$is_batch);
				$message = array();
				$message['error'] = false;
                $message['results'] = $result;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});

	$app->post('/batchresultpaths', function(Request $request, Response $response) {
		if (!haveEmptyParameters(('result_set_id'), $response)) {
			$request_data = $request->getParsedBody();

			
			$resultSetId = $request_data['result_set_id'];
		
			
            $db = new IamResultPaths;

			$imagePaths = $db->getImagePaths($resultSetId);
			$resultPaths = $db->getResultPaths($resultSetId);
				$message = array();
				$message['error'] = false;
				$message['imagePaths'] = $imagePaths;
				$message['resultPaths'] = $resultPaths;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});


	$app->post('/discoverpipelines', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('user_id'), $response)) {
			$request_data = $request->getParsedBody();

			
			$userid = $request_data['user_id'];
			
			
            $db = new DiscoverPipelines;

			$result = $db->getPipelines($userid);
				$message = array();
				$message['error'] = false;
                $message['results'] = $result;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});


	$app->post('/discoverpipelineversions', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('pipeline_id'), $response)) {
			$request_data = $request->getParsedBody();

			
			$pipeline_id = $request_data['pipeline_id'];
			
			
            $db = new DiscoverPipelineVersions;

			$result = $db->getPipelineVersions($pipeline_id);
				$message = array();
				$message['error'] = false;
                $message['results'] = $result;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});


	$app->post('/discovermodels', function(Request $request, Response $response) {
		if (!haveEmptyParameters(array('user_id'), $response)) {
			$request_data = $request->getParsedBody();

			
			$userid = $request_data['user_id'];
			
			
            $db = new DiscoverModels;

			$result = $db->getModelInfo($userid);
				$message = array();
				$message['error'] = false;
                $message['results'] = $result;
				$response->getBody()->write(json_encode($message));
				return $response
									->withHeader('Content-type', 'application/json')
									->withStatus(200);
		}

		
	});




	

	function haveEmptyParameters($required_params, $response) {
		$error = false;
		$error_params = '';
		$request_params = $_REQUEST;

		foreach ($required_params as $param) {
			if(!isset($request_params[$param]) || strlen($request_params[$param]) <= 0) {
				$error = true;
        $error_params .= $param . ', ';
      }
    }

    if($error) {
			$error_detail = array();
      $error_detail['error'] = true;
      $error_detail['message'] = 'Required parameters ' . substr($error_params, 0, -2) . ' are either missing or empty';

			$response->getBody()->write(json_encode($error_detail));
    }

    return $error;
	}

    $app->run();
    

    ?>